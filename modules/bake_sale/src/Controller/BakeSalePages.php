<?php

/**
 * Defines the BakeSalePages class.
 */
class BakeSalePages {


  /**
   * Display a summary of all pre-order items.
   */
  public static function itemSummary() {

    drupal_set_title(t('Bake Sale Item Totals'));

    $query = db_select('paragraphs_item', 'line_item');
    $query->condition('field_name', 'field_order');

    $query->join("field_data_field_order", "field_order",
      "line_item.item_id = field_order.field_order_value AND field_order.entity_type = 'entityform' AND field_order.bundle='hfc_bake_sale_pre_order_form' AND field_order.deleted = 0"
    );

    // Get the target nid of the bake_sale_item ordered in this line item.
    $query->join("field_data_field_order_item", "order_item",
      "line_item.item_id = order_item.entity_id AND line_item.revision_id = order_item.revision_id AND order_item.entity_type = 'paragraphs_item'"
    );
    $query->addField('order_item', 'field_order_item_target_id', 'tid');

    // Get the node title for the targeted item.
    $query->join("node", "n", "order_item.field_order_item_target_id = n.nid");
    $query->addField('n', 'title', 'title');

    // Get the quantity ordered for this line item.
    $query->join("field_data_field_quantity", "qty",
      "line_item.item_id = qty.entity_id AND line_item.revision_id = qty.revision_id AND qty.entity_type = 'paragraphs_item'"
    );
    $query->addField('qty', 'field_quantity_value', 'quantity');

    $result = $query->execute()->fetchAll();

    $items = [];
    $titles = [];

    foreach ($result as $item) {
      $quantity = !empty($items[$item->tid]['quantity']['data'])
        ? $items[$item->tid]['quantity']['data'] + (int) $item->quantity
        : (int) $item->quantity;
      $items[$item->tid] = [
        'title' => ['class' => ['title'], 'data' => check_plain($item->title)],
        'quantity' => ['class' => ['quantity'], 'data' => $quantity],
      ];
      $titles[$item->tid] = drupal_strtolower(check_plain($item->title));
    }

    // Sort $items array by $titles values.
    array_multisort($titles, SORT_ASC, $items);

    return [
      '#theme' => 'table',
      '#header' => [t('Item'), t('Quantity')],
      '#rows' => $items,
      '#empty' => t('No items ordered yet!'),
    ];
  }
}
