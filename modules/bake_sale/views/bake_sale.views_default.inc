<?php

/**
 * @file
 * Loads default views.
 *
 * This file is automatically loaded by Drupal.
 *
 * @see hook_views_default_views()
 */

/**
 * Implements hook_views_default_views().
 */
function bake_sale_views_default_views() {
  $views = [];
  $files = file_scan_directory(drupal_get_path('module', 'bake_sale') . '/views/default', '/.inc$/');
  foreach ($files as $filepath => $file) {
    require $filepath;
    if (isset($view)) {
      $views[$view->name] = $view;
    }
  }
  return $views;
}
