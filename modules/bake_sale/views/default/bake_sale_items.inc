<?php

$view = new view();
$view->name = 'bake_sale_items';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Bake Sale Items';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Holiday Stock Up Bake Sale';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['grouping'] = array(
  0 => array(
    'field' => 'field_food_type',
    'rendered' => 1,
    'rendered_strip' => 0,
  ),
);
$handler->display->display_options['style_options']['columns'] = array(
  'title' => 'title',
  'field_food_type' => 'field_food_type',
  'field_price' => 'field_price',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_food_type' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_price' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Field: Content: Food Type */
$handler->display->display_options['fields']['field_food_type']['id'] = 'field_food_type';
$handler->display->display_options['fields']['field_food_type']['table'] = 'field_data_field_food_type';
$handler->display->display_options['fields']['field_food_type']['field'] = 'field_food_type';
$handler->display->display_options['fields']['field_food_type']['label'] = '';
$handler->display->display_options['fields']['field_food_type']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_food_type']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_food_type']['element_wrapper_type'] = 'h3';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
/* Field: Content: Price */
$handler->display->display_options['fields']['field_price']['id'] = 'field_price';
$handler->display->display_options['fields']['field_price']['table'] = 'field_data_field_price';
$handler->display->display_options['fields']['field_price']['field'] = 'field_price';
$handler->display->display_options['fields']['field_price']['label'] = '';
$handler->display->display_options['fields']['field_price']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_price']['settings'] = array(
  'thousand_separator' => ',',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
);
/* Sort criterion: Content: Food Type (field_food_type) */
$handler->display->display_options['sorts']['field_food_type_value']['id'] = 'field_food_type_value';
$handler->display->display_options['sorts']['field_food_type_value']['table'] = 'field_data_field_food_type';
$handler->display->display_options['sorts']['field_food_type_value']['field'] = 'field_food_type_value';
/* Sort criterion: Content: Title */
$handler->display->display_options['sorts']['title']['id'] = 'title';
$handler->display->display_options['sorts']['title']['table'] = 'node';
$handler->display->display_options['sorts']['title']['field'] = 'title';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'bake_sale_item' => 'bake_sale_item',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['label'] = 'Bake Sale Information';
$handler->display->display_options['header']['area']['content'] = 'Tuesday, November 21 - 10am-2pm
Wednesday, November 22 10am-2pm & 5pm-7pm
Pick up will be next to the Fifty-One O One Restaurant

**[Pre-Order here through noon Sunday, November 19](/bake-sale/form)**
No time to pre-order? Stop by on the 21st or 22nd, plenty to choose from.

Desserts and breads prepared by HFC culinary students have become a tradition in many homes. Bring abundance and flavor to your holiday table with our delicious creations and give thanks for the extra time you will have to concentrate on dinner, your family and friends.
';
$handler->display->display_options['header']['area']['format'] = 'markdown';
$handler->display->display_options['path'] = 'bake-sale';

/* Display: Entity Reference */
$handler = $view->new_display('entityreference', 'Entity Reference', 'entityreference_1');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'entityreference_style';
$handler->display->display_options['style_options']['search_fields'] = array(
  'title' => 'title',
  'field_food_type' => 0,
  'field_price' => 0,
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'entityreference_fields';
$handler->display->display_options['defaults']['row_options'] = FALSE;
