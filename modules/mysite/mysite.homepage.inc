<?php

/**
 * @file
 * Includes block content callback for the homepage menu items.
 */

/**
 * Returns homepage (cuisine) menu block contents.
 */
function mysite_homepage_menu() {
  $output = array();


  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'menu')
    ->propertyCondition('status', NODE_PUBLISHED)
    ->fieldCondition('field_menu_dates', 'value2', time() - 86400, '>=')
    ->fieldCondition('field_menu_type', 'value', 'weekly')
    ->fieldOrderBy('field_menu_dates', 'value', 'ASC')
    ->range(0,1)
    ;
  $result = $query->execute();

  if (!empty($result['node'])) {
    $node = reset($result['node']);

    $output[] = array(
      '#prefix' => '<div id="front-menu-this-week">',
      '#markup' => views_embed_view('this_weeks_menu_front', 'block_entree', $node->nid),
      '#suffix' => '</div>',
    );
    $output[] = array(
      '#prefix' => '<div id="front-menu-standard">',
      '#markup' => views_embed_view('front_standard_menu', 'block_entree'),
      '#suffix' => '</div>',
    );
  }

  return $output;
}
