<?php

/**
 * @file
 * Defines front_standard_menu view.
 */

$view = new view();
$view->name = 'front_standard_menu';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Front Page Standard menu';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Standard menu';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['label'] = 'title';
$handler->display->display_options['header']['area']['content'] = '<h3>Entrées</h3>';
$handler->display->display_options['header']['area']['format'] = 'filtered_html';
/* Relationship: Entity Reference: Referenced Entity */
$handler->display->display_options['relationships']['field_entree_dish_target_id']['id'] = 'field_entree_dish_target_id';
$handler->display->display_options['relationships']['field_entree_dish_target_id']['table'] = 'field_data_field_entree_dish';
$handler->display->display_options['relationships']['field_entree_dish_target_id']['field'] = 'field_entree_dish_target_id';
$handler->display->display_options['relationships']['field_entree_dish_target_id']['label'] = 'entree_dish';
/* Field: Content: Price */
$handler->display->display_options['fields']['field_price']['id'] = 'field_price';
$handler->display->display_options['fields']['field_price']['table'] = 'field_data_field_price';
$handler->display->display_options['fields']['field_price']['field'] = 'field_price';
$handler->display->display_options['fields']['field_price']['relationship'] = 'field_entree_dish_target_id';
$handler->display->display_options['fields']['field_price']['label'] = '';
$handler->display->display_options['fields']['field_price']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_price']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_price']['settings'] = array(
  'thousand_separator' => ' ',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
);
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['relationship'] = 'field_entree_dish_target_id';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['title']['alter']['text'] = '[title] <em>([field_price])</em>';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['element_wrapper_class'] = 'menu-item';
$handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
/* Field: Content: Body */
$handler->display->display_options['fields']['body']['id'] = 'body';
$handler->display->display_options['fields']['body']['table'] = 'field_data_body';
$handler->display->display_options['fields']['body']['field'] = 'body';
$handler->display->display_options['fields']['body']['relationship'] = 'field_entree_dish_target_id';
$handler->display->display_options['fields']['body']['label'] = '';
$handler->display->display_options['fields']['body']['element_type'] = 'p';
$handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['body']['hide_alter_empty'] = FALSE;
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Contextual filter: Content: Nid */
$handler->display->display_options['arguments']['nid']['id'] = 'nid';
$handler->display->display_options['arguments']['nid']['table'] = 'node';
$handler->display->display_options['arguments']['nid']['field'] = 'nid';
$handler->display->display_options['arguments']['nid']['default_action'] = 'default';
$handler->display->display_options['arguments']['nid']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['nid']['default_argument_options']['argument'] = '156';
$handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'menu' => 'menu',
);
/* Filter criterion: Content: Dish Type (field_dish_type_term) */
$handler->display->display_options['filters']['field_dish_type_term_tid']['id'] = 'field_dish_type_term_tid';
$handler->display->display_options['filters']['field_dish_type_term_tid']['table'] = 'field_data_field_dish_type_term';
$handler->display->display_options['filters']['field_dish_type_term_tid']['field'] = 'field_dish_type_term_tid';
$handler->display->display_options['filters']['field_dish_type_term_tid']['relationship'] = 'field_entree_dish_target_id';
$handler->display->display_options['filters']['field_dish_type_term_tid']['value'] = array(
  2 => '2',
  1 => '1',
  3 => '3',
);
$handler->display->display_options['filters']['field_dish_type_term_tid']['type'] = 'select';
$handler->display->display_options['filters']['field_dish_type_term_tid']['vocabulary'] = 'dish_type';
/* Filter criterion: Content: Nid */
$handler->display->display_options['filters']['nid']['id'] = 'nid';
$handler->display->display_options['filters']['nid']['table'] = 'node';
$handler->display->display_options['filters']['nid']['field'] = 'nid';
$handler->display->display_options['filters']['nid']['value']['value'] = '156';

/* Display: Lighter Fare */
$handler = $view->new_display('block', 'Lighter Fare', 'block_lighter_fare');
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['label'] = 'title';
$handler->display->display_options['header']['area']['content'] = '<h3>Lighter Fare</h3>
All sandwiches and salads are served with a cup of soup du jour.
(additional charge of .75 for French Onion Au Gratin)';
$handler->display->display_options['header']['area']['format'] = 'filtered_html';
$handler->display->display_options['block_description'] = 'Standard Menu Light Fare';

/* Display: Entrées */
$handler = $view->new_display('block', 'Entrées', 'block_entree');
$handler->display->display_options['defaults']['css_class'] = FALSE;
$handler->display->display_options['css_class'] = 'front-standard-menu';
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: Global: View area */
$handler->display->display_options['header']['view']['id'] = 'view';
$handler->display->display_options['header']['view']['table'] = 'views';
$handler->display->display_options['header']['view']['field'] = 'view';
$handler->display->display_options['header']['view']['label'] = 'Lighter Fare';
$handler->display->display_options['header']['view']['view_to_insert'] = 'front_standard_menu:block_lighter_fare';
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['label'] = 'title';
$handler->display->display_options['header']['area']['content'] = '<h3>Entrées</h3>
All entrées are served with a cup of soup du jour and salad bar.
(additional charge of .75 for French Onion Au gratin)';
$handler->display->display_options['header']['area']['format'] = 'filtered_html';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'menu' => 'menu',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Dish Type (field_dish_type_term) */
$handler->display->display_options['filters']['field_dish_type_term_tid']['id'] = 'field_dish_type_term_tid';
$handler->display->display_options['filters']['field_dish_type_term_tid']['table'] = 'field_data_field_dish_type_term';
$handler->display->display_options['filters']['field_dish_type_term_tid']['field'] = 'field_dish_type_term_tid';
$handler->display->display_options['filters']['field_dish_type_term_tid']['relationship'] = 'field_entree_dish_target_id';
$handler->display->display_options['filters']['field_dish_type_term_tid']['value'] = array(
  4 => '4',
  5 => '5',
  6 => '6',
);
$handler->display->display_options['filters']['field_dish_type_term_tid']['group'] = 1;
$handler->display->display_options['filters']['field_dish_type_term_tid']['type'] = 'select';
$handler->display->display_options['filters']['field_dish_type_term_tid']['vocabulary'] = 'dish_type';
/* Filter criterion: Content: Nid */
$handler->display->display_options['filters']['nid']['id'] = 'nid';
$handler->display->display_options['filters']['nid']['table'] = 'node';
$handler->display->display_options['filters']['nid']['field'] = 'nid';
$handler->display->display_options['filters']['nid']['value']['value'] = '156';
$handler->display->display_options['filters']['nid']['group'] = 1;
$handler->display->display_options['block_description'] = 'Front Page Standard Menu';
