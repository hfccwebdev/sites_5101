jQuery(document).ready(function($) {

      // Sticky Menu

    var stickyNavTop = $('#section-navigation').offset().top;
    var stickyNav = function(){
      var scrollTop = $(window).scrollTop();

      if (scrollTop > stickyNavTop) {
        $('#section-navigation').addClass('sticky');
      }
      else {
        $('#section-navigation').removeClass('sticky');
      }
    };

  if ($(document.body).hasClass('front')) {
    // Removing active class from links on initial load
    $("#main-menu li").removeClass('active');

    // Smooth Scroll

    var hashTagActive = "";
      $("#main-menu a").click(function (event) {
        $("#main-menu li").removeClass('active');
        $(this).parent().addClass('active'); //adding active class to clicked li.
        if ($(this.hash).offset() != null) { //fixes menu for non-front pages.
          if(hashTagActive != this.hash) { //this will prevent if the user click several times the same link to freeze the scroll.
            event.preventDefault();
            //calculate destination place
            var dest = 0;
            if ($(this.hash).offset().top > $(document).height() - $(window).height()) {
              dest = $(document).height() - $(window).height();
            } else {
              dest = $(this.hash).offset().top;
            }
            //go to destination
            $('html,body').animate({
              scrollTop: dest
            }, 1000, 'swing');
            hashTagActive = this.hash;
          }
        }
      });

    stickyNav();

  }

  $(window).scroll(function() {
    stickyNav();
  });

});
