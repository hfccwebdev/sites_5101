<?php

/**
 * @file
 * Theme implementation for this_week_menu_header views block.
 *
 * @see block.tpl.php
 * @see template_preprocess()
 * @see template_preprocess_block()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<?php print $content ?>
